terraform {
  backend "s3" {
    bucket         = "pkottam-tfstate-devops"
    key            = "recipe-app.tfstate"
    region         = "us-west-2"
    encrypt        = true
    dynamodb_table = "pkottam-tfstate-lock"
  }
}

provider "aws" {
  region  = "us-west-2"
  version = "~> 2.55.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}